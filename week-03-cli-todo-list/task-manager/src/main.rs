use clap::{Parser, Subcommand};
use std::{fmt::Display, io::Write};
fn main() {
    let arg = Args::parse();

    println!("Hello, {:?}!", arg);

    let mut tasks: Vec<Task> = vec![];

    loop {
        print!(">");
        std::io::stdout().flush().unwrap();

        let mut line = String::new();

        std::io::stdin().read_line(&mut line).unwrap();

        let com = RuntimeArgs::parse_from(std::iter::once("pad").chain(line.split_whitespace()));

        match com.sub {
            SubCom::Exit => {
                let output = get_output(&tasks, &arg.name);
                if let Some(path) = &arg.file {
                    std::fs::write(path, output).unwrap();
                }
                return;
            }
            SubCom::Add(task_name) => {
                let new_task = Task::new(task_name.name);
                if tasks.contains(&new_task) {
                    println!("Task already exists");
                    continue;
                }

                tasks.push(new_task);
            }
            SubCom::Remove(task_name) => {
                let target_task = Task::new(task_name.name);

                if !tasks.contains(&target_task) {
                    println!("Task not found");
                    continue;
                }

                tasks.retain(|task| task != &target_task);
            }
            SubCom::List => {
                println!("{}", get_output(&tasks, &arg.name));
            }
            SubCom::Progress(task_name) => {
                let target_task = Task::new(task_name.name);

                if !tasks.contains(&target_task) {
                    println!("Task not found");
                    continue;
                }

                let task = tasks.iter_mut().find(|task| task == &&target_task).unwrap();

                task.status = Status::Progress;
            }
            SubCom::Finish(task_name) => {
                let target_task = Task::new(task_name.name);

                if !tasks.contains(&target_task) {
                    println!("Task not found");
                    continue;
                }

                let task = tasks.iter_mut().find(|task| task == &&target_task).unwrap();

                task.status = Status::Done;
            }
        }
    }
}

fn get_output(tasks: &[Task], owners_name: &str) -> String {
    let mut output = String::new();

    output.push_str(&format!("{}'s TODOs\n", owners_name));
    output.push_str("-------\n");

    tasks.iter().for_each(|task| {
        output.push_str(&format!("{} {}\n", task.status, task.name));
    });

    output
}

struct Task {
    name: String,
    status: Status,
}

impl Task {
    fn new(name: String) -> Self {
        Self {
            name,
            status: Status::default(),
        }
    }
}

impl PartialEq for Task {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

#[derive(Default)]
enum Status {
    #[default]
    Todo,
    Progress,
    Done,
}

impl Display for Status {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let status = match self {
            Status::Todo => "TODO",
            Status::Progress => "PROGRESS",
            Status::Done => "DONE",
        };

        write!(f, "{}", status)
    }
}

#[derive(Parser, Debug)]
struct Args {
    #[clap(short, long)]
    name: String,
    #[clap(short, long)]
    file: Option<String>,
}

#[derive(Debug, Parser)]
struct RuntimeArgs {
    #[clap(subcommand)]
    sub: SubCom,
}

#[derive(Debug, Subcommand)]
enum SubCom {
    Exit,
    Add(AddComm),
    Remove(RemoveComm),
    List,
    Progress(ProgressComm),
    Finish(FinishComm),
}

#[derive(Debug, Parser)]
struct AddComm {
    name: String,
}

#[derive(Debug, Parser)]
struct RemoveComm {
    name: String,
}

#[derive(Debug, Parser)]
struct ProgressComm {
    name: String,
}

#[derive(Debug, Parser)]
struct FinishComm {
    name: String,
}
